import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Week8Lab {

    public void start()
    {
        CodonTable table = new CodonTable();

        Scanner scanner = new Scanner( System.in );
        System.out.println( "Enter a codon to translate: " );
        String codon = scanner.nextLine();

        while( !codon.equalsIgnoreCase("quit") )
        {
            ArrayList<String> codons = new ArrayList<>();

            for( int i = 0; i < codon.length(); i += 3 )
            {
                codons.add( codon.substring( i, i + 3 ) );
            }
            //"I know you all are insta famous because you're comp sci students"
            Iterator<String> itr = codons.iterator();

            System.out.print( "The amino acid squence is " );
            while( itr.hasNext() )
            {

                String acid = table.getAminoAcidSequence(itr.next());

                if (acid == null)
                {
                    System.out.print( "<INVALID>" );
                }
                else
                {
                    System.out.print( acid );
                }
            }

            System.out.println( );

            System.out.println( "Enter a codon to translate: " );
            codon = scanner.nextLine();

        }
    }

    public static void main(String[] args)
    {
        Week8Lab lab = new Week8Lab();
        lab.start();
    }
}
